# Data-driven Vibration Control for AO Systems
<!-- Badges -->
[![License: MIT][license-badge]](/LICENSE)
[![Maintainter: Vaibhav Gupta][maintainer-badge]](mailto:vaibhav.gupta@epfl.ch)
[![DOI][DOI-badge]][DOI_link]  

This toolbox gives a vibration controller for a given atmospheric disturbance PSD as an IIR filter. The code has been used for

```bibtex
@inproceedings{gupta_DatadrivenIQCMicrodisturbance_2023,
  title     = {Direct Data-Driven Vibration Control for Adaptive Optics}, 
  booktitle = {2023 62th IEEE Conference on Decision and Control (CDC)},
  author    = {Gupta, Vaibhav and Karimi, Alireza and Wildi, François and Véran, Jean-Pierre},
  year      = {2023},
  address   = {Singapore},
  doi       = {10.1109/CDC49753.2023.10383356}
}
```

<!----------------------------------------------------------------------------->
## Required Softwares

- **MATLAB** (2019b or newer)
  - Control System Toolbox
  - Signal Processing Toolbox
  - Statistics and Machine Learning Toolbox
- **Toolbox manager** (`tbxmanager`) for MATLAB
  - yalmip
  - sedumi
- **MOSEK** version 9.2 or higher (not necessary but recommended)

### Installation Instructions

#### Toolbox manager

- Run script [`install_tbxmanager.m`](install_tbxmanager.m) to install Toolbox manager with all required modules.

#### Mosek

- Download appropiate installer from [MOSEK](https://www.mosek.com/downloads/) website and install the software.
- For academic uses, a [free license](https://www.mosek.com/products/academic-licenses/) could be requested. For commercial purposes, a 30-day [trial license](https://www.mosek.com/products/trial/) is also available.

<!----------------------------------------------------------------------------->
## Basic Usage Instructions

Class [`VibrationController`](@VibrationController/VibrationController.m) is the main class used for designing the vibration controller.

Code snippet for designing a data-driven controller:

```matlab
datadriven_controller = VibrationController("data", ...
    "Fs"     , Fs     , ...
    "tau_lag", tau_lag, ...
    "solver" , solver   ...
);
datadriven_controller.set_disturbance_frd(frd_model);
datadriven_controller.set_flux_noise_rms(flux_noise_rms);
datadriven_controller.update_parameter(...
    "gain"      , controller_gain     , ...
    "order"     , controller_order    , ...
    "bandwidth" , controller_bandwidth, ...
    "alpha"     , controller_alpha    );
datadriven_controller.design();
```

<!----------------------------------------------------------------------------->
## Usage Instructions

Set the current folder to the `src` folder.

- To generate the data and plots from the section "Simulation Example", run `main_SimulationExample.m`.
- To generate the data and plots from the section "Experimental Results", run `main_ExperimentalResults.m`.

***More Info:***

- Refer to [this README](+utility_functions/README.md) for usage instructions on the utility functions.
- Refer to [this README](example_scripts/README.md) for usage instructions on the example scripts.

<!----------------------------------------------------------------------------->
<!-- Markdown Links -->
[license-badge]: https://img.shields.io/gitlab/license/ddmac/paper-codes/vibration-control-for-ao-systems?label=License&gitlab_url=https%3A%2F%2Fgitlab.epfl.ch
[maintainer-badge]: https://img.shields.io/badge/Maintainer-Vaibhav_Gupta-blue.svg
[DOI-badge]: https://img.shields.io/badge/DOI-10.1109/CDC49753.2023.10383356-blue.svg
[DOI_link]: https://doi.org/10.1109/CDC49753.2023.10383356
