function set_continuous_model(obj)
%set_continuous_model Set the continuous-time model of the system 
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    % G1 (WFS + WFC)
    s = tf('s');
    obj.WFS = (1 - exp(-obj.Ts * s)) / (obj.Ts * s);
    obj.WFC = exp(-obj.tau_lag * s);

    % ZOH
    ZOH = (1 - exp(-obj.Ts * s)) / s;

    % G2 (DM)
    HF_filter = tf(1);
    DM_mirror = tf(1);
    obj.DM = DM_mirror * HF_filter;
end