function K = design_datadriven(obj, verbose)
%design_datadriven Data-driven vibration controller design
%
%   design_datadriven(obj) designs the controller in verbose mode.
%
%   design_datadriven(___, false) designs the controller in silent mode.
%
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    arguments
        obj
        verbose (1, 1) logical = true;
    end
    
    if isempty(obj.disturbance_frd)
        warning("VibrationController:design:disturbance_psd", "\n\t%s", ...
            "Disturbance FRD object is not defined.", ...
            "Assuming band-limited white noise with rms of 1 [mas]." ...
        );
        obj.disturbance_frd = tf(1 * 2 * obj.Ts);
    end

    if obj.flux_noise_rms == 0
        warning("VibrationController:design:flux_noise_rms", "\n\t%s", ...
            "RMS value of expected flux noise is set to 0.", ...
            "Ignoring effects of flux noise." ...
        );
    end
    
    z = tf('z', obj.Ts);
    plant_model = frd(...
            freqresp(obj.DM , obj.parameters.w) .* ...
            freqresp(obj.WFC, obj.parameters.w) .* ...
            freqresp(obj.WFS, obj.parameters.w), ...
        obj.parameters.w, obj.Ts);
    

    %% Initial Controller
    K_init = obj.parameters.gain / (z - 1);

    %% Data driven Controller
    omega_b = obj.parameters.bandwidth*2*pi;   % bandwidth

    W_d = obj.disturbance_frd;
    val = freqresp(W_d, omega_b);
    W_d = W_d / abs(val);
    W_d = W_d / obj.flux_noise_rms / obj.parameters.alpha;
    
    W_n = tf(1);

    % To match the defination used in the library
    W_n = frd(...
            freqresp(W_n , obj.parameters.w) .* ...
            freqresp(obj.DM, obj.parameters.w) .* ...
            freqresp(obj.WFC, obj.parameters.w), ...
        obj.parameters.w, obj.Ts);

    [num, den] = tfdata(K_init, 'v');
    den(obj.parameters.order + 1) = 0; % zero padding
    num(obj.parameters.order + 1) = 0; % zero padding
    
    Fx = 1;
    Fy = [1, -1];
    
    num_new = deconv(num, Fx);
    den_new = deconv(den, Fy);
    
    [SYS, OBJ, CON, PAR] = datadriven.utils.emptyStruct(); % load empty structure
    ctrl = struct( ...
        'num', num_new, ...
        'den', den_new, ...
        'Ts', obj.Ts, ...
        'Fx', Fx, ...
        'Fy', Fy); % assemble controller
    
    SYS.controller = ctrl;
    SYS.model = plant_model;
    SYS.W = obj.parameters.w;
    
    OBJ.o2.W1 = W_d;
    OBJ.o2.W3 = W_n;

    % OBJ.oinf.W1 = tf(0.5);
    CON.W1 = tf(0.5);
    
    PAR.maxIter = 200;
        
    tic
    [controller, ~] = datadriven.datadriven(SYS, OBJ, CON, PAR, verbose, obj.solver);
    elapsedTime = toc;
    
    if verbose
        fprintf("Controller found in %.6f seconds.\n", elapsedTime)
    end

    K = datadriven.utils.toTF(controller);
    % K = minreal(K, 1e-3);
    
end
