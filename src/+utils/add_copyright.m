function add_copyright(target, year, name, type, options)
%ADD_COPYRIGHT Adds copyright info to the `.m` files in the current directory
% 
%    ADD_COPYRIGHT() adds copyright to all `.m` files in current directory with
%       current year, default name and default copyright type.
% 
%    ADD_COPYRIGHT(target) adds copyright to all `.m` files in target directory
%       or in target `.m` file with current year, default author name and
%       default copyright type.
% 
%    ADD_COPYRIGHT(..., year, name, type) adds copyright with specified year,
%       author name and copyright type.
%
%   ----------------------------------------------------------------------------
%   Description:
%       Adds copyright info to the `.m` files in the current directory and all
%       the subfolders. The copyright line looks like,
%           %   Copyright <YEAR> <NAME> - <TYPE>
%       e.g.
%           %   Copyright 2022 John Doe - CC BY 4.0
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    arguments
        target = []
        year (1, 1) string = ""
        name (1, 1) string = "Vaibhav Gupta, DDMAC, EPFL"
        type (1, 1) string = "MIT"
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
        options.logger = []
    end
    
    if isempty(options.logger)
        options.logger = @(type, varargin) utils.logger_verbose(options.verbosity, type, varargin{:});
        options.logger("START", mfilename);
        cleanUp = onCleanup(@() options.logger("STOP", mfilename));
    end
    
    curdir = pwd;
    if isempty(target)
        target = curdir;
    end
    
    if strcmpi(year, "")
        year = string(datetime("today", "Format", "yyyy"));
    end
    
    copyright_string = sprintf("%%   Copyright %s %s (%s License)", year, name, type);

    
    if exist(target, "file") == 2
        add_copyright_to_file(dir(target), copyright_string, options.logger);
    elseif exist(target, "dir") == 7
        [~, fname, ~] = fileparts(target);
        options.logger("INFO", "Target is a dir! Adding copyright to `%s` and its subfolders ...", fname);
        files = get_files(target);
        arrayfun(@(file) add_copyright_to_file(file, copyright_string, options.logger), files);
        
        subfolders = get_subfolders(target);
        arrayfun(...
            @(subfolder) utils.add_copyright(...
                fullfile(subfolder.folder, subfolder.name), year, name, type, ...
                "verbosity", options.verbosity, "logger", options.logger), ...
            subfolders);
        
    else
        options.logger("WARN", "Target is not a `.m` file or a folder! Exiting!", target);
    end
end

%% Helper Functions
function add_copyright_to_file(file_dir, copyright_string, logger)
%ADD_COPYRIGHT_TO_FILE Adds copyright info to the file
% 

    [~, name, ext] = fileparts(file_dir.name);

    % Skip over any file that is not a `.m` file or is "contents.m"
    if strcmpi(name, "contents") || ~strcmpi(ext, ".m")
        logger("WARN", "`%s` is not a `.m` file! Skipping...", name);
        return          
    end
    logger("INFO", "Adding copyright to `%s` ...", name);

    org_file = fullfile(file_dir.folder, file_dir.name);
    tmp_file = fullfile(file_dir.folder, file_dir.name + ".tmp");

    %% Read lines and add/update copyright info
    fid_1 = fopen(org_file,'r+');
    fid_2 = fopen(tmp_file,'w');

    found_copyright = false;
    docstring_block = false;

    next_line = fgets(fid_1);
    while ischar(next_line)
        if ~found_copyright
            if startsWith(next_line, "%   Copyright", "IgnoreCase", true)
                % Found a copyright in docstring block
                found_copyright = true;

                % Replace line by copyright_string
                next_line = sprintf("%s\n", copyright_string);
            elseif docstring_block && ~startsWith(next_line, "%")
                % Exiting the docstring block
                docstring_block = false;

                % Write the copyright info
                fprintf(fid_2, "%s\n", ...
                    "%   ----------------------------------------------------------------------------", ...
                    copyright_string, ...
                    "%");
                found_copyright = true;
            elseif startsWith(next_line, "%")
                % Currently in docstring block
                docstring_block = true;
            end
        end
        fprintf(fid_2, "%s", next_line);    % copy fid_1 into fid_2
        next_line = fgets(fid_1);
    end
    fclose(fid_1);
    fclose(fid_2);
    
    %% Move tmp file to replace original file
    moved = movefile(tmp_file, org_file, 'f');
    if ~moved
        delete(tmp_file)
        logger("WARN", "Could not move file! Copyright is not addded.");
    else
        logger("INFO", "Copyright added.");
    end
end

function files = get_files(folder)
    % All files & folder in root directory
    files = dir(folder);
    if isempty(files)
      return
    end
    
    % remove '.', '..' and other folders from the listing
    to_remove = arrayfun(@(x) strcmp(x.name, '.'), files) ...
        | arrayfun(@(x) strcmp(x.name, '..'), files) ...
        | [files.isdir].';
    files = files(~to_remove);
end

function subfolders = get_subfolders(folder)
    % All files & folder in root directory
    files = dir(folder);
    if isempty(files)
      return
    end
    
    % remove '.', '..' and other folders from the listing
    to_remove = arrayfun(@(x) strcmp(x.name, '.'), files) ...
        | arrayfun(@(x) strcmp(x.name, '..'), files);
    subfolders = files([files.isdir].' & ~to_remove);
end