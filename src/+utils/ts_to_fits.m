function ts_to_fits(ts, filename)
%ts_to_file Writes a `.fits` file for the timeseries
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    arguments
        ts (1, 1) timeseries
        filename (1, 1) string
    end

    filename = strcat(filename, ".fits");
    fitswrite([ts.Time, ts.Data], filename)

end