% Plots for Narrowband disturbance in dB
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

%% Cleanup ---------------------------------------------------------------------
clearvars; close all; clc;

%% Parameters ------------------------------------------------------------------
options = struct();
options.save_plot = true;
options.readout_noise = false;
options.colors = [ ...
    "#0072BD";
    "#D95319";
    "#EDB120";
    "#7E2F8E";
    "#77AC30";
];

Fs = 400;
n_photons = 1600;

if options.readout_noise
    datafolder = fullfile("data", "readout");
    plotfolder = fullfile("figures", "readout");
else
    datafolder = fullfile("data", "no_readout");
    plotfolder = fullfile("figures", "no_readout");
end

options.plot_folder = fullfile(plotfolder, "extra_narrowband_disturbance_db");

%% Load required libraries -----------------------------------------------------
MATLAB_environment_setup();

%% Generate datafiles if not available -----------------------------------------
for type = "narrowband"
    % Unknown
    utility_functions.extra_disturbance_study(...
        "design_extra", "none", ...
        "sim_extra", type, ...
        "readout_noise", options.readout_noise);
    % Known
    utility_functions.extra_disturbance_study(...
        "design_extra", type, ...
        "sim_extra", type, ...
        "readout_noise", options.readout_noise);
    % Wrong
    utility_functions.extra_disturbance_study(...
        "design_extra", type, ...
        "sim_extra", "none", ...
        "readout_noise", options.readout_noise);
end

%% Plot ------------------------------------------------------------------------
for extra_type = "narrowband"
    for study_type = ["known_disturbance", "unknown_disturbance", "wrong_disturbance"]
        datafile = fullfile( ...
            datafolder, ...
            "extra_disturbance_study", ...
            study_type, ...
            extra_type, ...
            sprintf("%04d_Hz", Fs), ...
            sprintf("%04d_photons.mat", n_photons));
        dataset = load(datafile);
        plot_helper_narrowband(dataset, study_type, options);
    end
end

%% Cleanup ---------------------------------------------------------------------
if options.save_plot
    clearvars; close all; clc;
end

%% Helper Functions ------------------------------------------------------------
function plot_helper_narrowband(dataset, study_type, options)
    %% Data Manipulation -------------------------------------------------------
    mag_space = unique(dataset.extra_space(:, 1));
    freq_space = unique(dataset.extra_space(:, 2));
    bw_space = unique(dataset.extra_space(:, 3));

    [freq_grid, bw_grid] = ndgrid(freq_space, bw_space);

    rms = struct();
    rms.raw = arrayfun(@(x) x.tip.raw.total, dataset.rms_data);
    rms.int = arrayfun(@(x) x.tip.int.total, dataset.rms_data);
    rms.type2 = arrayfun(@(x) x.tip.type2.total, dataset.rms_data);
    rms.data = arrayfun(@(x) x.tip.data.total, dataset.rms_data);

    improvement_grid_int = arrayfun(...
        @(x, y) mag2db(rms.int(all(dataset.extra_space(:, 2:3) == [x, y], 2), 1) ./ ...
            rms.data(all(dataset.extra_space(:, 2:3) == [x, y], 2), 1)), ...
        freq_grid, bw_grid, ...
        'UniformOutput', 0 ...
    );

    improvement_grid_type2 = arrayfun(...
        @(x, y) mag2db(rms.type2(all(dataset.extra_space(:, 2:3) == [x, y], 2), 1) ./ ...
            rms.data(all(dataset.extra_space(:, 2:3) == [x, y], 2), 1)), ...
        freq_grid, bw_grid, ...
        'UniformOutput', 0 ...
    );

    mean_improvement_int = cellfun(@(x) mean(x), improvement_grid_int);
    std_improvement_int = cellfun(@(x) std(x), improvement_grid_int);
    
    mean_improvement_type2 = cellfun(@(x) mean(x), improvement_grid_type2);
    std_improvement_type2 = cellfun(@(x) std(x), improvement_grid_type2);
    
    %% Plot Improvement --------------------------------------------------------
    fig = figure();
    old_pos = fig.Position;
    fig.Position = [old_pos(1:2),  840, 420];
    if options.save_plot
        fig.Renderer = "painters";
        fig.Units = "centimeters";
        fig.PaperSize = fig.Position(3:4);
        fig.PaperUnits = "normalized";
        fig.PaperPosition = [0, 0, 1, 1];
    end
    x_space = freq_space;

    t = tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
    xlabel(t, "Center Frequency of Narrowband Noise [Hz]");
    
    if all(mean_improvement_int >= 0, 'all') && all(mean_improvement_type2 >= 0, 'all')
        ylabel(t, "Attenuation Gain [dB]");
    elseif all(mean_improvement_int <= 0, 'all') && all(mean_improvement_type2 <= 0, 'all')
        ylabel(t, "Attenuation Loss [dB]");
    else
        ylabel(t, "Attenuation Gain/Loss [dB]");
    end
    
    title(t, regexprep(...
        lower(replace(study_type, "_", " narrowband ")), ...
        "(^|\.)\s*.", ...
        "${upper($0)}"));

    ax = nexttile();    
    hold on; grid on; box on;
    
    %%%
    b_improvement_type2 = bar(x_space, mean_improvement_type2, 0.8, "LineWidth", 1);
    for i_int = 1:length(b_improvement_type2)
        b_improvement_type2(i_int).FaceColor = options.colors(i_int);
        b_improvement_type2(i_int).FaceAlpha = 0.75;

        errorbar(b_improvement_type2(i_int).XEndPoints, ...
            b_improvement_type2(i_int).YEndPoints, ...
            std_improvement_type2(:, i_int), ...
            "LineStyle" , "none"    , ...
            "CapSize"   , 5         , ...
            "Color"     , "#000000" , ...
            "LineWidth" , 1           ...
        );
    end
    
    %%%
    b_improvement_int = bar(x_space, mean_improvement_int, 0.8, "LineWidth", 1);
    hatchbar(b_improvement_int);
    for i_improvement = 1:length(b_improvement_int)
        b_improvement_int(i_improvement).FaceColor = options.colors(i_improvement);
        b_improvement_int(i_improvement).FaceAlpha = 1;

        errorbar( ...
            b_improvement_int(i_improvement).XEndPoints, ...
            b_improvement_int(i_improvement).YEndPoints, ...
            std_improvement_int(:, i_improvement), ...
            "LineStyle" , "none"    , ...
            "CapSize"   , 5         , ...
            "Color"     , "#000000" , ...
            "LineWidth" , 1           ...
        );
    end
    
    if study_type == "known_disturbance"
        ylim([0, 30]);
    elseif study_type == "unknown_disturbance"
        ylim([-2, 4]);
    end
    
    xticks(x_space);
    
    h_improvement = legend(b_improvement_int, ...
        arrayfun(@(x) sprintf("%d Hz", x), bw_space), ...
        "Orientation", "vertical", ...
        "Location", "eastoutside");
    title(h_improvement, "Bandwidth");
    
    % Controller Legend
    a=axes("position", get(ax,"position"), "visible", "off");
    p_leg = [];
    p_leg(1) = patch([-inf, inf], [-inf, inf], 'w', "LineWidth", 1);
    p_leg(2) = patch([-inf, inf], [-inf, inf], 'w', "LineWidth", 1);
    h_leg1 = legend(a, p_leg, "Integrator", "Type2 Controller", ...
        "Orientation", "horizontal", ...
        "Location", "northwest");
    title(h_leg1, "Comparison against");
    drawnow;
    icon_transform = h_leg1.EntryContainer.NodeChildren(2).Icon.Transform;
    line(icon_transform,  ...
        [0:0.2:0.8; 0.2:0.2:1], repmat([0;1], 1, 5), ...
        "Color", "#444444", ...
        "LineWidth", 1);
    
    if options.save_plot
        if ~isfolder(options.plot_folder)
            mkdir(options.plot_folder)
        end
        plot_filename = fullfile( ...
            options.plot_folder, ...
            sprintf("%s_gain", ...
                replace(study_type, "_disturbance", "")) ...
        );
        print(fig, plot_filename, "-dpng", "-painters", "-r300");
        print(fig, plot_filename, "-dpdf", "-painters");

        fig.Color = "none";
        fig.InvertHardcopy = "off";
        print(fig, plot_filename, "-dsvg", "-painters");
        close(fig);
    end
end

function hatchbar(bar_handle, options)
    arguments
        bar_handle
        options.LineSpacing = 0.1
        options.LineWidth   = 1
        options.LineColor   = "#444444"
    end
    n_bars = length(bar_handle);
    
    if (n_bars == 1)
        bar_gap = repmat(bar_handle.XEndPoints(2) - bar_handle(1).XEndPoints(1), ...
            1, length(bar_handle.XEndPoints));
    else
        bar_gap = bar_handle(2).XEndPoints - bar_handle(1).XEndPoints;
    end
    spacing = max(arrayfun(@(x) max(abs(x.YEndPoints)), bar_handle)) * options.LineSpacing;
    spacing = min(spacing, 0.5);
    
    for i_bars = 1:n_bars
        y_top = bar_handle(i_bars).YEndPoints;
        
        if y_top < 0
            y_bottom = y_top;
            y_top = 0 * y_top;
        else
            y_bottom = 0 * y_top;
        end
        barwidth = bar_gap * bar_handle(i_bars).BarWidth;
        
        x_left = bar_handle(i_bars).XEndPoints - barwidth/2;

        n_group = length(x_left);
        for i_group = 1:n_group
            y_low = y_bottom(i_group);
            x_tmp = [];
            y_tmp = [];
            while y_low < y_top(i_group)
                dy_relative = (y_top(i_group) - y_low) / spacing;
                if dy_relative > 1
                    dy_relative = 1;
                end
                x_range = [0; barwidth(i_group)] * dy_relative;
                y_range = [0; spacing] * dy_relative;
                x_tmp = [x_tmp, x_left(i_group) + x_range];
                y_tmp = [y_tmp, y_low + y_range];          
                y_low = y_low + spacing;
            end
            line(x_tmp, y_tmp,...
                "LineWidth", options.LineWidth, ...
                "Color", options.LineColor);        
        end
    end
end
