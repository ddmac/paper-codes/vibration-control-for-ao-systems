# Example Scripts

Before proceding, perform one of the following:

* Add this folder to the MATLAB path
* Copy the scripts to the base folder of the toolbox

<!----------------------------------------------------------------------------->
## MATLAB environment setup

Update [`MATLAB_environment_setup.m`](MATLAB_environment_setup.m) to setup the environment conditions on your PC.

**Note:**

* For HPC (non-interactive sessions), always add path.
* For interactive sessions, adding only once should be enough.

<!----------------------------------------------------------------------------->
## Controller optimisation

Perform grid search for controller parameters for all desired environments (controller frequency and photon flux) using:

* [`search_integrator_env.m`](search_integrator_env.m)
* [`search_type2_env.m`](search_type2_env.m) (Intergrator should already be optimised)
* [`search_datadriven_env.m`](search_datadriven_env.m) (Intergrator should already be optimised)

Once, search has been done for all environments. Use following commands respectively to generate optimised paramter values.

```matlab
    utils.save_optimised_integrator_params()
    utils.save_optimised_type2_params()
    utils.save_optimised_datadriven_params()
```

**Note:** In case the search stops in middle, use [`find_missing_env.m`](find_missing_env.m) to find the remaining environments.

<!----------------------------------------------------------------------------->
## Compare controllers

Use [`compare_controllers.m`](compare_controllers.m) for quick comparison among the controllers under different simulation conditions.

**Note:** For this, optimisation for all controller parameters should have been performed.

<!----------------------------------------------------------------------------->
## Varying atmospheric study

[`run_controller_statistics.m`](run_controller_statistics.m) performs and plot results for effect of atmospheric variations on controller performance.

**Note:** For this, optimisation for all controller parameters should have been performed.
