function MATLAB_environment_setup(useSLURM)
%%MATLAB_environment_setup Setup libraries for MATLAB
%
%   MATLAB_environment_setup() sets MATLAB environment for normal usecases.
% 
%   MATLAB_environment_setup(true) sets MATLAB environment for SLURM.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%
    arguments
        useSLURM (1, 1) logical = false
    end

    if useSLURM
        % Add path to the `toolbox manager`
        addpath("/home/users/g/guptavai/tbxmanager");
        tbxmanager restorepath;
        
        % Path to mosek/fusion (if available)
        path_to_fusion = "";    % e.g.: "${MOSEK}\9.2\tools\platform\win64x86\bin\mosek.jar"
        fusion_chk = contains(javaclasspath('-dynamic'), "mosek", 'IgnoreCase', true);
        if strlength(path_to_fusion) && ~sum(fusion_chk)
            javaaddpath(path_to_fusion);
            fprintf("""Mosek Fusion"" added to the Matlab path\n");
        end
    else
        % Toolbox Manager - Yalmip, Sedumi, ...
        if ~contains(path, "yalmip", 'IgnoreCase', true)
            tbxmanager restorepath;
        end

        % Path to mosek/fusion (if available)
        path_to_fusion = "C:\Program Files\Mosek\9.2\tools\platform\win64x86\bin\mosek.jar";
        fusion_chk = contains(javaclasspath('-dynamic'), "mosek", 'IgnoreCase', true);
        if strlength(path_to_fusion) && ~sum(fusion_chk)
            javaaddpath(path_to_fusion);
            fprintf("""Mosek Fusion"" added to the Matlab path\n");
        end
    end
end
