 classdef Simulator < handle
%Simulator Class for closed-loop simulations
%
%   Simulator Properties:
%     * WFS        - Sensing component
%     * WFC        - Computation Delay component
%     * DM         - Actuation component
%     * controller - Vibration controller
%     * result     - Structure of the simulation results
%     * S          - Disturbance to error closed-loop transfer function
%     * U          - Noise to error closed-loop transfer function
% 
%   Simulator Methods:
%     * set_controller - Sets the vibration controller for the simulation
%     * run            - Runs the LTI simulation of the closed loop 
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%
    
    properties (SetAccess = private)
        WFS             % Sensing component
        WFC             % Computation Delay component
        DM              % Actuation component
        K               % Continuous-time controller
        controller      % Vibration controller
        result          % Structure of the simulation results
        disturbance_CL  % Disturbance to error closed-loop transfer function
        noise_CL        % Noise to error closed-loop transfer function
    end
    
    methods
        function obj = Simulator(Fs, tau_lag)
            [obj.WFS, obj.WFC, obj.DM] = obj.get_system_model(1/Fs, tau_lag);
        end

        function set_controller(obj, controller)
            %set_controller Sets the vibration controller for the simulation
            % 
            %   set_controller(obj, controller) sets the vibration controller
            %   and calculates the closed-loop transfer function.
            % 

            arguments
                obj
                controller (1, 1) VibrationController
            end

            obj.controller = controller;
            obj.K = obj.controller.K_DAC;
            
            obj.disturbance_CL = feedback(1, obj.DM * obj.K * obj.WFC * obj.WFS);
            obj.noise_CL = feedback(obj.K * obj.DM * obj.WFC, obj.WFS);
        end

    end
end