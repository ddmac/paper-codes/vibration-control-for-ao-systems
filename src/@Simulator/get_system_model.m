function [WFS, WFC, DM] = get_system_model(~, Ts, tau_lag)
%get_system_model Returns system model for simulations
% 
%   [WFS, WFC, DM] = get_system_model(~, Ts, tau_lag) returns the sensing
%   component (WFS), computation delay (WFC) and actuator component (DM).
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    s = tf('s');

    WFS = (1 - exp(-Ts * s)) / (Ts * s);
    WFC = exp(-tau_lag * s);    % computation lag

    HF_filter = tf(1);
    DM_mirror = tf(1);
    DM = DM_mirror * HF_filter;
end