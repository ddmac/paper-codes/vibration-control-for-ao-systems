function run(obj, ts_disturbance, ts_noise)
%run Runs the LTI simulation of the closed loop 
% 
%   run(obj, ts_disturbance, ts_noise) simulates the closed loop with given
%   disturbance and noise timeseries.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    arguments
        obj
        ts_disturbance  (1, 1) timeseries
        ts_noise        (1, 1) timeseries
    end
    
    obj.result = struct();

    try
        obj.result.disturbance = lsim(obj.disturbance_CL, ...
            ts_disturbance.Data, ...
            ts_disturbance.Time); 
    catch ME
        % Skip in case of error
        warning("Simulator:run:disturbance", ...
            "%s\n", ME.identifier, ME.message);
        obj.result.disturbance = NaN(length(ts_disturbance), 1); 
    end

    try
        obj.result.noise = lsim(obj.noise_CL, ts_noise.Data, ts_noise.Time);
    catch ME
        % Skip in case of error
        warning("Simulator:run:noise", ...
            "%s\n", ME.identifier, ME.message);
        obj.result.noise = NaN(length(ts_noise), 1); 
    end

    obj.result.total = obj.result.disturbance + obj.result.noise;
end

