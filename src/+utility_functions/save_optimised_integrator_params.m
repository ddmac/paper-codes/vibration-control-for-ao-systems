function save_optimised_integrator_params(options)
%save_optimised_integrator_params Optimised integrator controller parameters
% 
%   save_optimised_integrator_params(name, value) specifies the optional and
%   plotting parameters.
%
%   Name-Value Pair Arguments:
%     |----------------|-------------------------------------------------------|
%     | Name           | Description                                           |
%     |----------------|-------------------------------------------------------|
%     | readout_noise  | If detector has a readout noise                       |
%     |                |   Default: false                                      |
%     |----------------|-------------------------------------------------------|
%     | plot           | To plot the graphs for optimisation                   |
%     |                |   Default: true                                       |
%     |----------------|-------------------------------------------------------|
%     | save_plot      | Autosave the graphs for optimisation                  |
%     |                |   Default: true                                       |
%     |----------------|-------------------------------------------------------|
% 
%   ----------------------------------------------------------------------------
%   Copyright 2023 Vaibhav Gupta, DDMAC, EPFL (MIT License)
%

    arguments
        options.readout_noise (1, 1) logical = false
        options.plot          (1, 1) logical = false
        options.save_plot     (1, 1) logical = false
    end

    %% Basic Specifications ----------------------------------------------------
    if options.readout_noise
        datafolder = fullfile("data", "readout");
        plotfolder = fullfile("figures", "readout");
    else
        datafolder = fullfile("data", "no_readout");
        plotfolder = fullfile("figures", "no_readout");
    end

    % Save file for optimised results        
    parent_folder = fullfile( ...
        datafolder, ...
        "optimise_integrator");

    options.plot_folder = fullfile( ...
        plotfolder, ...
        "optimise_integrator"); 

    nd_grid_file = fullfile(parent_folder, "nd_grid.mat");
    params_file = fullfile(parent_folder, "optimised_params.mat");

    %% Extract n-d grid --------------------------------------------------------
    
    %%% Read Datafiles
    fs_folders = dir(fullfile( ...
        parent_folder, ...
        "*_Hz"));

    n_fs_folders = length(fs_folders);

    Fs_space = [];
    optimised_params = struct();
    for i_fs_folder = 1:n_fs_folders
        Fs = double(extractBefore(...
            string(fs_folders(i_fs_folder).name), ...
            "_Hz"));
        Fs_space = [Fs_space; Fs];

        photon_files = dir(fullfile( ...
            parent_folder, ...
            sprintf("%04d_Hz", Fs), ...
            "*_photons.mat"));

        photon_space = arrayfun( ...
            @(x) double(extractBefore(string(x.name), "_photons.mat")), ...
            photon_files); % It is same for all folders

        n_photon_files = length(photon_files);
        
        for ii = 1:n_photon_files
            data = load(fullfile(photon_files(ii).folder, photon_files(ii).name));
            data.param_space = data.gain_grid';

            for DoF = ["tip", "tilt"]
                rms_data = arrayfun(@(x) get_rms_values(x, DoF), data.rms_data);
                [~, idx] = min(rms_data);
                optimised_params.(DoF)(i_fs_folder, ii) = data.param_space(idx);
            end
        end
    end


    %% Get optimised parameters ------------------------------------------------
    get_optimal_gain = struct();
    for DoF = ["tip", "tilt"]
        get_optimal_gain.(DoF) = griddedInterpolant(...
            {Fs_space, photon_space}, ...
            optimised_params.(DoF));
    end

    %% Save optimised parameters -----------------------------------------------
    save(params_file, ...
        "optimised_params", ...
        "get_optimal_gain", ...
        "Fs_space", ...
        "photon_space");

    %% Helper Functions --------------------------------------------------------
    function rms_value = get_rms_values(rms_data, DoF)
        if isempty(rms_data.(DoF))
            % If simulations are not run
            rms_value = inf;
        else
            % Extract the total residual for integrator controller
            rms_value = rms_data.(DoF).int.total;
        end
    end

end